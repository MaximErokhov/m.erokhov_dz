# -*- coding: utf-8 -*- 

from django.db import models

class Speakers(models.Model):

	speaker_name = models.CharField(max_length=100)
	description = models.CharField(max_length=300)

	def __unicode__(self):
		return self.speaker_name
# Create your models here.
