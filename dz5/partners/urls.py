from django.conf.urls import patterns, url
from partners import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dz5.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', views.index, name='partners_index'),
)
