# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0002_auto_20150504_1804'),
    ]

    operations = [
        migrations.AddField(
            model_name='partners',
            name='description',
            field=models.CharField(default=datetime.datetime(2015, 5, 6, 12, 48, 49, 921004, tzinfo=utc), max_length=300),
            preserve_default=False,
        ),
    ]
