# -*- coding: utf-8 -*- 

from django.db import models

class Partners(models.Model):
	part_name = models.CharField(max_length=100)
	description = models.CharField(max_length=300)
	img = models.ImageField()
	
	def __unicode__(self):
		return self.part_name
# Create your models here.
