from django.conf.urls import patterns, include, url
from contacts import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dz5.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', views.index, name='contacts_index'),
)
