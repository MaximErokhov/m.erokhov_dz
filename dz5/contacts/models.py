# -*- coding: utf-8 -*- 
from django.db import models

class Contacts(models.Model):
	name = models.CharField(max_length=100)
	email = models.EmailField()

	def __unicode__(self):
		return self.name
# Create your models here.
