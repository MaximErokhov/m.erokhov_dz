from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.views.generic import TemplateView

admin.autodiscover()
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dz5.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^main/', include('django.contrib.flatpages.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^news/', include('news.urls')),
    url(r'^contacts/', include('contacts.urls')),
    url(r'^partners/', include('partners.urls')),
	url(r'^speeches/', include('speeches.urls')),
	url(r'^speakers/', include('speakers.urls')),
	url(r'^organizers/', include('organizers.urls')),
 )   

from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:    
    urlpatterns += patterns('', 
    url(r'^404$', TemplateView.as_view(template_name='404.html')),
    url(r'^500$', TemplateView.as_view(template_name='500.html')),)