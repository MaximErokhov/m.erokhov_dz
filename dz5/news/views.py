from django.shortcuts import render
from .models import News
from django.views.decorators.cache import cache_page

@cache_page(60 * 15)
def index(request):
	news_list = News.objects.all()
	context = {'news': news_list}
	return render(request, 'news/index.html', context)


