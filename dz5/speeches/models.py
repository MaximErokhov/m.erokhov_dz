# -*- coding: utf-8 -*- 

from django.db import models

class Speeches(models.Model):
	id = models.AutoField(primary_key=True)
	title = models.CharField(u'Заголовок', max_length=70)
	speaker_name = models.CharField(u'Имя спикера', max_length=100)
	topic = models.CharField(u'Тема', max_length=100)
	time = models.DateTimeField(u'Время')
	class Meta:
		verbose_name = 'Выступление' 
		verbose_name_plural = 'Выступления'
		
	def __unicode__(self):
		return self.title

# Create your models here.
