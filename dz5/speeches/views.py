# -*- coding: utf-8 -*- 
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.cache import cache_page
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Speeches
from .forms import SpeechesForm

def index(request):
    speeches_list = Speeches.objects.all()
    paginator = Paginator(speeches_list, 9) 
    page = request.GET.get('page')
    
    try:
        speeches = paginator.page(page)
    except PageNotAnInteger:
        speeches = paginator.page(1)
    except EmptyPage:
        speeches = paginator.page(paginator.num_pages)

    context = {'speeches': speeches}
    return render(request, 'speeches/index.html', context)


# def index1(request):
# 	speeches_list = Speeches.objects.all()
# 	context = {'speeches': speeches_list}
# 	return render(request, 'speeches/index.html', context)


def edit(request, speeches_id):

	speech = Speeches.objects.get(id = speeches_id)
	
	if request.method == "POST":
		form = SpeechesForm(request.POST, instance = speech)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect( reverse('speeches_index'))

	else: 
		form = SpeechesForm(instance = speech)
	context = {'form': form}
	return render(request, 'speeches/edit.html', context)
	

def add(request):
	
	if request.method == "POST":
		form = SpeechesForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect( reverse('speeches_index'))
			
	else: 
		form = SpeechesForm()
	context = {'form': form}
	return render(request, 'speeches/add.html', context)

def delete(request, speeches_id):
	speech = Speeches.objects.get(id = speeches_id)
	speech.delete()
	return HttpResponseRedirect( reverse('speeches_index'))

            
	
