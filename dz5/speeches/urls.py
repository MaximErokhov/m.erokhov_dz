from django.conf.urls import patterns, url
from speeches import views

urlpatterns = patterns('',
   

    url(r'^$', views.index, name='speeches_index'),
    url(r'^(?P<speeches_id>\d+)/edit/$', views.edit, name='speeches_edit'),
    url(r'^(?P<speeches_id>\d+)/delete/$', views.delete, name='speeches_delete'),
    url(r'^\d/add/$', views.add, name='speeches_add'),
)
