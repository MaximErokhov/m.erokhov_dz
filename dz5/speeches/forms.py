# -*- coding: utf-8 -*- 

from django.forms import ModelForm
from .models import Speeches

class SpeechesForm(ModelForm):
	class Meta:
		model = Speeches
		fields = ['id','title', 'speaker_name', 'topic', 'time']



