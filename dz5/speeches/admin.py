from django.contrib import admin
from .models import Speeches

class SpeechesAdmin(admin.ModelAdmin):
    search_fields = ['title', 'speaker_name', 'topic']
class SpeechesAdmin(admin.ModelAdmin):
	list_display =  ('title', 'topic')

admin.site.register(Speeches, SpeechesAdmin)
# Register your models here.
