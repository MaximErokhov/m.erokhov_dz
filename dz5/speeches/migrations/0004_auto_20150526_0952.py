# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('speeches', '0003_auto_20150525_2056'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='speeches',
            options={'verbose_name': '\u0412\u044b\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u0435', 'verbose_name_plural': '\u0412\u044b\u0441\u0442\u0443\u043f\u043b\u0435\u043d\u0438\u044f'},
        ),
        migrations.AlterField(
            model_name='speeches',
            name='speaker_name',
            field=models.CharField(max_length=100, verbose_name='\u0418\u043c\u044f \u0441\u043f\u0438\u043a\u0435\u0440\u0430'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='speeches',
            name='time',
            field=models.DateTimeField(verbose_name='\u0412\u0440\u0435\u043c\u044f'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='speeches',
            name='title',
            field=models.CharField(max_length=70, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='speeches',
            name='topic',
            field=models.CharField(max_length=100, verbose_name='\u0422\u0435\u043c\u0430'),
            preserve_default=True,
        ),
    ]
