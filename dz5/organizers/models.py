# -*- coding: utf-8 -*- 

from django.db import models

class Organizers(models.Model): 
	name = models.CharField(u'Название Организатора', max_length=100)
	role = models.CharField(u'Роль', max_length=150)

	def __unicode__(self):
		return self.name
# Create your models here.
