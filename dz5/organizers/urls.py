from django.conf.urls import patterns, url
from organizers import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dz5.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', views.index, name='organizers_index'),
)
