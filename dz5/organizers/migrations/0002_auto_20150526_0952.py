# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('organizers', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organizers',
            name='name',
            field=models.CharField(max_length=100, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u041e\u0440\u0433\u0430\u043d\u0438\u0437\u0430\u0442\u043e\u0440\u0430'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='organizers',
            name='role',
            field=models.CharField(max_length=150, verbose_name='\u0420\u043e\u043b\u044c'),
            preserve_default=True,
        ),
    ]
